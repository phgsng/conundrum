/* The output of below program is:
  
        val
        ref
        !!!
  
   Q1: What is the output if you comment trait impls one, two, or
       three?
  
   Q2: It is possible to comment two trait impls at the same time.
       Which two? What does it print?
  
   Q3: What is going on here?
*/
struct S;
trait T { fn t(self); }

impl T for   S { fn t(self) { println!("val"); } } /* one */
impl T for  &S { fn t(self) { println!("ref"); } } /* two */
impl T for &&S { fn t(self) { println!("!!!"); } } /* three */

fn main () {
    (  S).t();
    ( &S).t();
    (&&S).t();
}
