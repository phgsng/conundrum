/*
# Riddle #5: Expressing a Statement

Any of these function obviously returns only once, but it
is not the same ``return`` that is being executed in each
case. What’s the difference?

Playpen link for the error messages:
https://play.rust-lang.org/?version=nightly&mode=debug&edition=2018&gist=d2b44f4289c28385c3cc04a983de2a18
*/

fn foo() { return  return  return  } //         ^^^^^^^^------
fn bar() { return; return; return; } // ------  ^^^^^^^
fn baz() { return; return  return; } // ------  ^^^^^^^^^^^^^^
fn main() { foo(); bar(); baz(); }
