/*! # Rust riddle no. 3.

    Define equality relations over types using specialization.
    This is accomplished by overloading the ``PartialEq`` trait
    to return false for ``Wrap<T>`` compared to any other wrapped
    type. This serves as the default implementation [0] that we
    overload with a method returning true for each concrete type
    that we define.

    *Question*: There is a commented trait impl:

        impl PartialEq<Wrap<A>> for Wrap<A> { … }

    that looks like it should be a functional overload as it
    specializes PartialEq for a ``Wrap<A>`` [1]. However rustc
    rejects the program if we use this alternative instead of
    the one bounded by the rather useless trait ``TA``:

        62 |     assert!(foo != baz);
           |                    ^^^ expected struct `A`, found struct `B`
           |
           = note: expected struct `Wrap<A>`
                      found struct `Wrap<B>`

    ``Wrap<A>`` and ``Wrap<B>`` being distinct types, we would
    expect that case to be covered by the default impl which rustc
    blatantly ignores! Why is that?


    [0] An unstable feature only available on the nightly toolchain:

            $ rustup toolchain install nightly
            $ cargo +nightly run
        
        or visit the playpen:
        https://play.rust-lang.org/?version=nightly&mode=debug&edition=2018&gist=779eff1d62000e0a250f9a2a158637cb

    [1] This is the default for the type parameter of ``PartialEq``
        so it doesn’t actually need to be stated explicitly.
 */
#![feature(specialization)]

use std::cmp::PartialEq;

struct A;
struct B;
struct Wrap<T>(T);

trait TA { }      /* Marker trait. */
impl TA for A { } /* Needed for the trait bounded version below. */

/** Inequality: Wrap<X> != Wrap<Y> for any two X, Y. */
impl<T, U> PartialEq<Wrap<T>> for Wrap<U> {
    default fn eq(&self, _: &Wrap<T>) -> bool { false }
}

/** Equality: Wrap<A> == Wrap<A> – with bound TA. */
impl<T: TA> PartialEq<Wrap<T>> for Wrap<A> {
    fn eq(&self, _: &Wrap<T>) -> bool { true }
}

/** Equality: Wrap<A> == Wrap<A> – with same type in lhs and rhs.
    Uncommenting this and commenting the bounded version above causes
    a compile error in the assertion marked below.

    Why? */
//impl PartialEq<Wrap<A>> for Wrap<A> {
//    fn eq(&self, _: &Wrap<A>) -> bool { true }
//}

fn main() {
    let foo = Wrap(A);
    let bar = Wrap(A);
    let baz = Wrap(B);

    assert!(foo == foo);
    assert!(foo == bar);
    assert!(foo != baz); /* does not compile with “same type” */
}
