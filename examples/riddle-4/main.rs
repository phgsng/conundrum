#![allow(unused_variables)]
fn main () {
    mystery_0();
    mystery_1();
    mystery_2();
    mystery_3();
}

struct A(&'static str);

impl Drop for A {
    fn drop(&mut self) { println!("{}", self.0) }
}

fn mystery_0 () {
    println!("## 0 ##");
    let (_, a) = (A("foo"), A("bar"));
    println!("baz");
}

fn mystery_1 () {
    println!("## 1 ##");
    let (_, _) = (A("foo"), A("bar"));
    println!("baz");
}

fn mystery_2 () {
    println!("## 2 ##");
    let c = (A("foo"), A("bar"));
    let (_, a) = c;
    println!("baz");
}

fn mystery_3 () {
    println!("## 3 ##");
    let c = (A("foo"), A("bar"));
    let (_, _) = c;
    println!("baz");
}
