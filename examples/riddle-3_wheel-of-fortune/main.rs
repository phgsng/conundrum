/*! Implementation of the Intra2net *Kitchen Fairy Wheel of Fortune*
    in the type system of Rust.

    - Maps relation to the type system.
    - Consistency checked at compile time, fails to build
      if a kitchen fairy or transition is missing.
    - No runtime overhead whatsoever.

    - Downside: no runtime luxury like iteration or dynamic dispatch
      whatsoever.

    **Caution**:
    This uses unstable features which may void your compiler’s
    warranty!
*/
#![allow(unused)]
#![feature(specialization)]
use std::cmp::{Eq, PartialEq};

/** A struct generic over fairies to represent a slice on the wheel
    of fortune. */
#[derive(Debug)] struct Wheel<Fairy>(Fairy);

trait WheelNext<N> { }

/** Helper to generate the type and trait definitions and impls for
    each slice defined later. */
macro_rules! WheelSlice {
    ($cur:ident -> $nxt:ident) => {
        /** In the type system, each fairy will be represented as a struct. */
        #[derive(Debug)] struct $cur;

        /** Each fairy’s name is written to a slice on the wheel. */
        impl Wheel<$cur> {
            /** Each friday, we invoke the current fairy’s ``friday()``
                method to obtain the dish washing fairy for next week.

                Note that ``self`` is taken by value to render the current
                instance inaccessible; it should be obvious that there cannot
                be two fairies laboring in the kitchen at the same time. */
            fn friday(self) -> Wheel<$nxt> { Wheel($nxt) }
        }
        
        /** Specialization is driven by this trait that is custom for
            each ``Fairy``. */
        impl WheelNext<$nxt> for $cur { }

        /** A ``Fairy`` is always equal to itself, type wise anyways … */
        impl<T> PartialEq<Wheel<T>> for Wheel<$cur>
            where T: WheelNext<$nxt>
        {
            fn eq(&self, _: &Wheel<T>) -> bool { true }
        }

        /* This impl expresses type level identity for fairies but
           mysteriously it won’t compile! */
//      impl PartialEq<Wheel<$cur>> for Wheel<$cur>
//      {
//          fn eq(&self, _: &Wheel<$cur>) -> bool { true }
//      }

        impl Eq for Wheel<$cur> { } /* Goodie: reflexivity. */
    }
}

/** All fairies are different. */
impl<T, U> PartialEq<Wheel<T>> for Wheel<U> {
    default fn eq(&self, _: &Wheel<T>) -> bool { false }
}

WheelSlice!(Steffen   -> Tom);
WheelSlice!(Tom       -> Julie);
WheelSlice!(Julie     -> Philipp);
WheelSlice!(Philipp   -> Gerd);
WheelSlice!(Gerd      -> Flo);
WheelSlice!(Flo       -> Erich);
WheelSlice!(Erich     -> Christian);
WheelSlice!(Christian -> Moritz);
WheelSlice!(Moritz    -> Steffen);

fn main() {
    let cur = Wheel(Gerd);  /* Assume this week was Gerd’s turn. */
    let cur = cur.friday(); /* Crank the wheel! */
    /* Who’s the lucky fairy of next week’s kitchen duty. */
    println!("Kitchen fairy this week: {:?}", cur);
    println!("Is it Gerd? -- {:?}", cur == Wheel(Gerd));
    println!("Is it Flo? -- {:?}", cur == Wheel(Flo));
    let cur = cur.friday(); /* Crank. */
    println!("Is it not Gerd next week? -- {:?}", cur == Wheel(Gerd));
    if (cur != Wheel(Gerd)) {
        println!("Well who is it then? -- {:?}", cur);
    }
}
