=================================================================
                          Rust Riddles
=================================================================

For the code see the ``examples/`` subdir. To compile and run the
individual riddles, invoke Cargo like so: ::

    $ cargo run --example riddle-2

Have fun and report bugs or mistakes at your discretion!

